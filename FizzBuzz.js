const fizz = "FIZZ"
const buzz = "BUZZ"
const fizzbuzz = "FIZZBUZZ"

function checker(number){
    if( divisibilitybyThree(number) ) {
        if( divisibilitybyFive(number)) {
            return fizzbuzz;
        }
        else {
            return fizz;
        }
    }
    else if( divisibilitybyFive(number)){
        return buzz;
    }
    else{
        return number;
    }
}

function divisibilitybyThree(number){
    if(number%3 === 0){
        return true;
    }
    else{
        return false;
    }
}

function divisibilitybyFive(number){
    if(number%5 === 0){
        return true;
    }
    else{
        return false;
    }
}

module.exports = checker;
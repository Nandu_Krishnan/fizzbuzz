const expect = require('chai').expect;
const fizzBuzz = require('../FizzBuzz');

describe('FizzBuzz', function () {

   /* beforeEach('initialize', () => {
        console.log("hello");
    });

    afterEach('after', () => {
        console.log("After");
    }); */

    it('should return number for normal numbers', () => {
        let result = fizzBuzz(1);
        expect(result).to.be.equal(1);
        expect(result).to.be.a('number');
    });

    it('should return fizz for 3', () => {
        let result = fizzBuzz(3);
        expect(result).to.be.equal('FIZZ');
    });

    it('should return buzz for 5', () => {
        let result = fizzBuzz(5);
        expect(result).to.be.equal('BUZZ');
    });

    it('should return fizz for 15', () => {
        let result = fizzBuzz(15);
        expect(result).to.be.equal('FIZZBUZZ');
    });

})